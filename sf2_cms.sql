-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 17, 2014 at 12:09 AM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sf2_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` longtext,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  `autoslag` longtext,
  `slag` varchar(255) DEFAULT NULL,
  `slagtrue` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `body`, `title`, `thumbnail`, `active`, `publish_date`, `temp1`, `temp2`, `temp3`, `autoslag`, `slag`, `slagtrue`) VALUES
(1, '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus libero justo, lobortis tincidunt tincidunt at, cursus id quam. Nunc pulvinar sit amet dui quis pellentesque. Phasellus eu neque pellentesque, imperdiet lacus et, semper lacus. Quisque vel sodales velit, ac eleifend diam. Aliquam maximus commodo ligula non posuere. Sed efficitur iaculis malesuada. Donec non ultricies nibh. Cras id mi urna. Vestibulum quis eros id felis porttitor luctus quis in felis. Suspendisse pharetra ornare gravida. In tellus erat, porta id vehicula eget, condimentum in tortor. Fusce lacinia, nibh vel maximus venenatis, leo enim tempus eros, eu scelerisque leo tortor nec tellus. Nam et magna eros.</p>\r\n<p>\r\n	Suspendisse potenti. Pellentesque auctor metus nec sapien eleifend, sit amet tincidunt nunc maximus. Fusce dictum feugiat rutrum. Sed ultrices lacus nec enim ultrices placerat. Aliquam molestie malesuada orci eget tincidunt. Curabitur laoreet malesuada tellus at eleifend. Aliquam magna leo, iaculis eu erat quis, varius molestie ipsum. Nullam et mi sit amet tellus congue pellentesque. Donec dictum tortor nec metus fringilla pharetra. Donec sit amet imperdiet tellus. Nulla tincidunt, mauris non consectetur mollis, purus eros varius justo, non commodo ex magna id leo. Curabitur orci arcu, luctus non leo vel, scelerisque consectetur tellus. Fusce pellentesque eleifend justo, in fringilla nunc rhoncus sit amet. Donec purus sapien, tristique nec velit eu, fringilla venenatis lectus. Cras hendrerit facilisis purus, eu eleifend metus condimentum vel. Vestibulum elementum mi sed nunc consectetur, mattis feugiat turpis molestie.</p>\r\n<p>\r\n	Aenean pulvinar est sed quam mattis, ut ullamcorper velit tincidunt. Nulla sit amet molestie lectus. Etiam vel risus vitae nulla sagittis scelerisque a non libero. Duis dui turpis, rhoncus vitae leo a, auctor viverra lectus. Aenean efficitur lacus ac felis condimentum, sed ornare lectus ultricies. Mauris eget sagittis tellus. Phasellus vitae fermentum nulla. Donec quis ex scelerisque, euismod justo non, finibus nisl.</p>\r\n<p>\r\n	<img alt="" src="/ckfinder/userfiles/images/val.jpg" style="width: 575px; height: 860px;" /></p>', 'CMS driven by Symfony 2', NULL, NULL, '2012-04-12 11:07:35', NULL, NULL, NULL, 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-phasellus-libero-justo-lobortis-tincidunt-tin', 'cms-driven-by-symfony-2', 1),
(2, '<p>\r\n	Phasellus fringilla nunc et varius varius. Curabitur nec faucibus tortor, sit amet porttitor orci. Donec efficitur est id elit fringilla vulputate. Nam eu diam consectetur, mattis felis vitae, commodo eros. Phasellus feugiat sodales nunc, at lacinia nisi porta a. Aliquam erat dui, ornare a ornare ut, commodo id leo. Nulla facilisis et est sagittis laoreet.</p>\r\n<p>\r\n	<img alt="" src="/ckfinder/userfiles/images/400337_337202859636313_264754440214489_1156936_945733333_n.jpg" style="width: 320px; height: 182px;" /></p>\r\n<p>\r\n	Etiam congue ante vel elit consequat sagittis. Suspendisse laoreet nisi eget eros dapibus, eget mattis est auctor.</p>\r\n<p>\r\n	Phasellus malesuada et nulla a accumsan. Nunc interdum commodo sapien, eget fringilla lectus feugiat a. Aliquam magna elit, tincidunt eget ligula egestas, sodales eleifend dolor. Maecenas ornare convallis erat, vitae luctus tortor egestas ac. Phasellus at odio tempus, ullamcorper ligula ac, dapibus elit. Ut rutrum tortor ut tellus cursus, sit amet vulputate ante accumsan. Nullam eget justo pretium, vestibulum odio et, rutrum magna. Aliquam porttitor cursus urna ut malesuada. Pellentesque sagittis ornare nisi at vehicula. Fusce congue placerat condimentum. Nam laoreet lorem at velit tincidunt, id euismod odio fringilla. Phasellus elementum tortor vel velit lobortis, quis imperdiet tortor condimentum. Aenean laoreet fermentum quam, non dictum metus bibendum eu. Suspendisse consequat venenatis nisl sed elementum.</p>', 'About Us', NULL, NULL, '2012-04-12 11:08:52', NULL, NULL, NULL, 'phasellus-fringilla-nunc-et-varius-varius-curabitur-nec-faucibus-tortor-sit-amet-porttitor-orci-done', 'chariots-opening-hours', 0),
(3, '<p>\r\n	Vivamus elit metus, dapibus vitae dignissim ac, fringilla vitae velit. Donec vestibulum nec lectus eget sodales. Phasellus eu ultricies erat. Ut felis enim, lobortis in turpis eget, dapibus laoreet eros. Donec pulvinar orci nisi, eget iaculis velit molestie sit amet. Pellentesque pharetra et sem ac porttitor. Mauris in eleifend nulla. Cras felis leo, egestas sed commodo sit amet, lacinia at elit. Fusce aliquet arcu ac ligula convallis aliquet. Phasellus aliquet elit ullamcorper metus ullamcorper, eu vehicula mi convallis. Phasellus placerat cursus massa, ac tincidunt odio consequat ac. Donec viverra ipsum id elit consectetur, id fringilla enim vehicula. Etiam at est enim.</p>\r\n<p>\r\n	Quisque dictum semper risus et condimentum. Vivamus maximus consectetur ipsum at iaculis. Sed consequat placerat orci, et iaculis sem tempor eget. Aenean eget nunc interdum, maximus lacus eu, feugiat magna. Donec vitae urna felis. Nam tincidunt libero eget posuere laoreet. Mauris mollis non nisi vitae bibendum. Cras quis sapien nec quam aliquam luctus. Vestibulum laoreet egestas augue quis molestie.</p>\r\n<p>\r\n	Mauris et odio tristique, laoreet mauris sit amet, varius eros. Donec cursus eget metus eu rhoncus. Curabitur ultrices vitae nunc vitae tempor. Mauris faucibus bibendum neque in vestibulum. Cras tempus diam nisl. Donec id porttitor sapien, et euismod lectus. Morbi consequat libero sit amet purus molestie hendrerit. In et convallis tellus. Donec lobortis vel diam et aliquet. Sed lectus eros, pharetra et eleifend eget, vestibulum vitae lacus. Donec imperdiet sapien et arcu interdum, vel condimentum augue tristique. Maecenas non diam ut ante dignissim dictum. Ut sagittis diam sapien, vitae laoreet neque laoreet et. Quisque blandit sapien a ex dignissim rhoncus. Maecenas non orci in eros eleifend faucibus ut vitae nulla. Quisque non enim mi.</p>\r\n<p>\r\n	Aenean elementum nulla velit, sit amet ultricies erat convallis non. Mauris ac fermentum dolor. In sit amet ligula elementum, auctor nulla vitae, pellentesque turpis. Maecenas velit nunc, elementum in mollis non, finibus et nunc. Etiam ut dui dignissim, aliquam lectus nec, hendrerit leo. Vivamus id ligula vestibulum, porttitor ipsum porttitor, porta magna. Nunc vestibulum velit vitae mi maximus porttitor. Nunc auctor porta blandit. Pellentesque vitae ex ac mauris convallis malesuada vel non tellus. Sed egestas posuere blandit. Sed sed auctor magna. Suspendisse feugiat vulputate ante nec pulvinar. Proin suscipit eros eget lectus malesuada, ac viverra nisl euismod.</p>', 'Links', NULL, NULL, '2012-04-16 19:49:15', NULL, NULL, NULL, 'vivamus-elit-metus-dapibus-vitae-dignissim-ac-fringilla-vitae-velit-donec-vestibulum-nec-lectus-eget', 'mini-bus-hire-in-harlow', 0),
(5, '<p class="single-first-p">\r\n	<img alt="" src="/ckfinder/userfiles/images/197605_197764416915123_130541460304086_616674_1632617_n.jpg" style="width: 400px; height: 267px;" /></p>\r\n<p class="single-first-p">\r\n	The purpose of any website is to get people from your target audience interested in what you&rsquo;re offering. Whether it be a product or service, 9 times out of 10, someone is going to want to communicate with you further. Because of this, in almost any industry, you&rsquo;re going to want to create a contact page.</p>\r\n<p>\r\n	For some, this is that last page on the site map where you just throw a bunch of information. You can leave it up to the person to decide how they want to contact you and what they want to contact you about. For others, this is the last attempt to get your potential customer to give you their business.</p>\r\n<p>\r\n	The contact page is much more important than many give it credit. Many basic websites just throw some numbers and e-mails up and move along. But in most cases, this is the page your customer sees before they decide they want you on their project. Or before they decide they want to visit you to purchase your product.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="fixed-empty-p">\r\n	&nbsp;</p>', 'Contacts', NULL, NULL, '2012-04-16 22:28:06', NULL, NULL, NULL, 'the-purpose-of-any-website-is-to-get-people-from-your-target-audience-interested-in-what-you-rsquo-r', 'mini-bus-hire-in-b-strotford', 0),
(16, '<p>\r\n	Although the measures announced in the Autumn Statement are coalition government policy, it emerged that Lib Dem Business Secretary Vince Cable had written to the OBR expressing &quot;growing concern&quot; that the difference between forecasts based on coalition policies and projections for future years - where spending decisions had not yet been made - was not being made clear.</p>\r\n<p>\r\n	In its response to the letter, the OBR said the distinction was made &quot;very clear&quot;, saying it based its forecasts on current government policy.</p>\r\n<p>\r\n	Speaking to Sky News, Mr Cable said he had no reservations about the Autumn Statement but emphasised that the Lib Dems had a &quot;different approach&quot; from that of their Conservative coalition colleagues on how to proceed with deficit reduction in the next parliament.</p>\r\n<p>\r\n	&quot;They are making claims about tax cuts we have said are simply not realisable. There will be an argument about the different mix of public spending and taxation,&quot; he said.</p>\r\n<p>\r\n	He added: &quot;It&#39;s very difficult to see how the Conservatives can achieve their objectives of protecting those core services and their policies on tax. They don&#39;t add up, there is a different view and we should be expressing it.&quot;</p>\r\n<p>\r\n	The Lib Dem business secretary has told his officials not to engage &quot;at this stage in rounds of negotiation on public spending cuts which are the proper responsibility of the next government&quot;.</p>\r\n<p>\r\n	Lib Dem Chief Secretary to the Treasury Danny Alexander told the BBC&#39;s Daily Politics borrowing was falling &quot;less fast than expected in March&quot;, putting this down to &quot;weaker than expected tax receipts&quot;.</p>\r\n<p>\r\n	However, he said the UK was doing &quot;far, far better&quot; than almost any other major economy.</p>', 'News', NULL, NULL, '2014-12-02 00:44:54', NULL, NULL, NULL, 'although-the-measures-announced-in-the-autumn-statement-are-coalition-government-policy-it-emerged-t', 'for_google_so_good', 0),
(17, 'You can edit this page by clicking "afgan menu" in menu.', 'afgan menu', NULL, NULL, '2014-12-03 19:13:52', NULL, NULL, NULL, 'auto-slag-afgan-menu', 'afgan-menu', NULL),
(18, '<p class="text small">\r\n	&quot;Ich habe mir das Video angesehen. So wie ein Familienangeh&ouml;riger es sich angucken w&uuml;rde&quot;, sagte New Yorks B&uuml;rgermeister Bill de Blasio nach der Entscheidung. &quot;Es war sehr traurig, sich das anzusehen.&quot;</p>\r\n<p class="text small">\r\n	Das mit einem Mobiltelefon aufgezeichnete Video eines Passanten erregt New York seit dem 17. Juli. Das ist der Tag, an dem Eric Garner an den Folgen eines Polizeieinsatzes starb. Es zeigt, wie sich eine Handvoll Polizisten auf ihn werfen und niederringen. Sie dr&uuml;cken seinen Kopf auf den Gehweg. Es zeigt aber vor allem, dass ein Polizist ihn in den W&uuml;rgegriff nimmt. Eine in New York schon lange verbotene Praxis.</p>\r\n<p class="text small">\r\n	Immer wieder sagte der 43-j&auml;hrige Familienvater: &quot;Ich bekomme keine Luft mehr.&quot; Schlie&szlig;lich verliert er das Bewusstsein, wird sp&auml;ter im Krankenhaus f&uuml;r tot erkl&auml;rt. Die Polizei warf Eric Garner vor, unversteuerte Zigaretten verkaufen zu wollen.</p>', 'Doiche firs', NULL, NULL, '2014-12-03 19:53:21', NULL, NULL, NULL, '-ich-habe-mir-das-video-angesehen-so-wie-ein-familienangeh-ouml-riger-es-sich-angucken-w-uuml-rde-sa', 'doiche-firs', 0),
(21, '<p class="introduction" id="story_continues_1">\r\n	The US Food and Drug Administration is considering lifting a ban on blood donations by men who have had sex with other men - even just once - since 1977. The blood is tested so what is the point of asking donors questions about their sexual history?</p>\r\n<p>\r\n	The ban was put in place as response to the spread of HIV and Aids in the gay community. But advances in testing and a better understanding of the disease mean the US is being urged to follow other countries, such as the UK, and allow gay men to donate blood as long if they have refrained from sex with another man for one year.</p>\r\n<p>\r\n	The 12-month deferral period is because it takes on average two to four weeks to pick up an HIV infection when testing blood and a couple of months to detect Hepatitis B. So the questions about a donor&#39;s sexual history filter out potential infections - although they inevitably mean people with healthy blood are not allowed to donate.</p>\r\n<p id="story_continues_2">\r\n	Although campaigners say they are enthusiastic about lifting the ban, they argue it does not go far enough. &quot;Our goal is to eliminate sexual orientation from the deferral process and instead base the decision on an individual risk assessment,&quot; says Ryan James Yesak, founder of the US National Gay Blood Drive.</p>\r\n<p>\r\n	He says male or female donors should instead be asked if they have had receptive anal intercourse in the last year. But Dr Steven Kleinman, senior medical adviser to the American Association of Blood Banks, says who you have sex with is a better risk indicator than what you&#39;re doing with that person.</p>\r\n<p>\r\n	And in the US, he says, men who have sex with men make up the group in which HIV prevalence is highest. &quot;Maybe the tool we use is crude - it&#39;s not a fine scalpel but more of a sledgehammer. But if we use a fine scalpel, we might miss some people.&quot;</p>', 'Supply lines', NULL, NULL, '2014-12-03 22:22:40', NULL, NULL, NULL, 'the-us-food-and-drug-administration-is-considering-lifting-a-ban-on-blood-donations-by-men-who-have-', 'supply-lines', 0),
(22, '<p>\r\n	Young and good-looking, Gidrat had been selected to posed in 1937 for a sculpture student after it was decided that the hall should be decorated with sculptures, representing all walks of Soviet life, including farmers, soldiers and scientists.</p>\r\n<p>\r\n	As well as being a high jumper, Arkady Gidrat taught at the State Central Institute of physical culture and was writing a PhD when war broke out.</p>\r\n<p>\r\n	He volunteered and was soon sent on an officer course aimed at raising a new generation of talented lieutenants, which the Soviet army lacked at the time.</p>\r\n<p>\r\n	His family never heard from him again. Arkady Gidrat went missing in September 1941.</p>\r\n<p>\r\n	For decades his wife and daughter paid their respects to his sculpture in the hall of the metro station. Every May, on Victory day, they would bring flowers. His daughter Olga remembers him as a wonderful father.</p>\r\n<p>\r\n	&quot;My father&#39;s fate was unknown. So this was the only place we could come to commemorate him,</p>\r\n<p>\r\n	&quot;I still remember him lifting me on his shoulders and carrying me around. One summer&#39;s day in 1941 he put me on his shoulders and then left and never returned.&quot;</p>', 'Missing in action', NULL, NULL, '2014-12-04 00:53:56', NULL, NULL, NULL, 'young-and-good-looking-gidrat-had-been-selected-to-posed-in-1937-for-a-sculpture-student-after-it-wa', 'missing-in-action', 0),
(23, '<div class="map-body">\r\n	<p>\r\n		In 1985, when Apple&#39;s board made Steve Jobs step down as the chief executive of the company he founded, he decided to leave and create the Next computer.</p>\r\n	<div class="hide-wrapper extra-content">\r\n		<p>\r\n			Next became the computer on which Tim Berners-Lee developed the world&#39;s first web server software.</p>\r\n		<p>\r\n			Many called the period between 1985 and 1996 - when Mr Jobs returned to Apple - the start of the &quot;digital revolution&quot;, when computers became commonplace and first appeared in homes.</p>\r\n		<p>\r\n			On his return, Mr Jobs brought Apple from near bankruptcy to profitability by 1998. Photographer Doug Menuez had rare access to this exciting time.</p>\r\n		<p>\r\n			<strong>You can see more from the </strong><span class="story-body__link">World Update team via its home page</span><strong> and </strong><span class="story-body__link-external">YouTube</span><strong>.</strong></p>\r\n		<p>\r\n			<i>Pictures: </i><span class="story-body__link-external">Doug Menuez </span></p>\r\n		<p>\r\n			<i>Producer: Megha Mohan </i></p>\r\n		<p>\r\n			<i>Presenter: Dan Damon</i></p>\r\n	</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', 'Quit Apple', NULL, NULL, '2014-12-04 00:58:38', NULL, NULL, NULL, 'in-1985-when-apple-39-s-board-made-steve-jobs-step-down-as-the-chief-executive-of-the-company-he-fou', 'quit-apple', 0),
(24, 'You can edit this page by clicking "Dan Damon" in submenu.', 'Dan Damon', NULL, NULL, '2014-12-04 01:07:52', NULL, NULL, NULL, 'auto-slag-dan-damon', 'dan-damon', NULL),
(25, 'You can edit this page by clicking "Jobs" in menu.', 'Jobs', NULL, NULL, '2014-12-04 01:48:57', NULL, NULL, NULL, 'auto-slag-jobs', 'jobs', NULL),
(26, '<p itemprop="articleBody">\r\n	El presidente Juan Manuel Santos celebro este mi&eacute;rcoles la reanudaci&oacute;n de los di&aacute;logos de paz de La Habana y afirm&oacute; que el nuevo ciclo, que comienza el 10 de diciembre, servir&aacute; de escenario para continuar las discusiones sobre c&oacute;mo desescalar el conflicto con las Farc y as&iacute; generar hechos que permitan fortalecer la confianza de la ciudadan&iacute;a en el proceso.</p>\r\n<p itemprop="articleBody">\r\n	Este tema lo vienen abordando las partes desde hace algunos meses en la mesa de Cuba, pero hasta el momento no se han dado pasos concretos que permitan bajar la intensidad de la guerra en territorio colombiano.</p>\r\n<p itemprop="articleBody">\r\n	&ldquo;Celebro que la mesa de La Habana se haya puesto de acuerdo para reanudar&rdquo;, precis&oacute; Santos, quien enfatiz&oacute; que el &uacute;ltimo ciclo de este a&ntilde;o, que va del 10 al 17 de diciembre, &ldquo;se van a dedicar a continuar las discusiones sobre c&oacute;mo desescalar el conflicto y qu&eacute; tipo de gestos se pueden ir mostrando&rdquo;.</p>', 'Principale', NULL, NULL, '2014-12-04 01:51:35', NULL, NULL, NULL, 'el-presidente-juan-manuel-santos-celebro-este-mi-eacute-rcoles-la-reanudaci-oacute-n-de-los-di-aacut', 'principale', 0),
(27, '<p>\r\n	Le conducteur de la voiture, Eric Robic, reconnu coupable d&rsquo;homicide involontaire aggrav&eacute;, a &eacute;t&eacute; condamn&eacute; &agrave; cinq ans de prison ferme. Le passager du v&eacute;hicule, Claude Khayat, &eacute;cope, lui, de 15 mois de prison ferme sans mandat de d&eacute;p&ocirc;t [cela signifie qu&#39;il n&#39;a pas &eacute;t&eacute; incarc&eacute;r&eacute; &agrave; l&#39;issue de l&#39;audience et pourra &eacute;ventuellement b&eacute;n&eacute;ficier d&#39;un am&eacute;nagement de peine, NDLR], pour non assistance &agrave; personne en p&eacute;ril. Les proches de Lee Zeitouni, venus d&rsquo;Isra&euml;l pour le proc&egrave;s, ont quitt&eacute; le Palais de justice de Paris en sanglots.</p>\r\n<p>\r\n	L&rsquo;affaire, qui a &quot;d&eacute;fray&eacute; la chronique judiciaire&quot; selon les termes de l&rsquo;avocat de la partie civile, Me Gilles-William Goldnadel, s&rsquo;est donc achev&eacute;e sous les lambris majestueux de la premi&egrave;re chambre du tribunal correctionnel de Paris, qui abritait pour l&rsquo;occasion la 10e chambre. Un lieu charg&eacute; d&rsquo;histoire puisque que c&rsquo;est dans cette salle que s&rsquo;est tenu l&rsquo;historique proc&egrave;s de Marie-Antoinette. Mercredi, &agrave; l&rsquo;endroit o&ugrave; se tenait jadis la reine envoy&eacute;e &agrave; l&rsquo;&eacute;chafaud, comparaissaient deux hommes impliqu&eacute;s dans un &quot;banal&quot; accident de la route.</p>', 'Nouvelles', NULL, NULL, '2014-12-04 01:52:50', NULL, NULL, NULL, 'le-conducteur-de-la-voiture-eric-robic-reconnu-coupable-d-rsquo-homicide-involontaire-aggrav-eacute-', 'nouvelles', 0),
(28, '<h2 class="subtitle small ">\r\n	Keine Anklage, kein Prozess</h2>\r\n<p class="text small">\r\n	Der Gerichtsmediziner stellte fest: Es war Totschlag aufgrund des W&uuml;rgegriffs und des Drucks auf die Brust. Dennoch erhebt das Geschworenengericht gegen den Polizisten, der den W&uuml;rgegriff angewandt hatte, keine Anklage. Es kommt - wie in Ferguson - nicht einmal zu einer ausf&uuml;hrlichen Untersuchung durch einen Prozess. Das hat der Bezirksstaatsanwalt inzwischen best&auml;tigt.</p>\r\n<p class="text small">\r\n	Im New Yorker Sender NY1 erkl&auml;rt Jonathan Moore, der Anwalt von Eric Garners Familie, nicht nur der W&uuml;rgegriff des Polizisten sei verwerflich. Die Anwendung t&ouml;dlicher Gewalt, um Eric Garner zu &uuml;berw&auml;ltigen, sei in jedem Fall unangemessen und exzessiv gewesen.</p>', 'Nachrichten', NULL, NULL, '2014-12-04 01:56:31', NULL, NULL, NULL, 'keine-anklage-kein-prozess-der-gerichtsmediziner-stellte-fest-es-war-totschlag-aufgrund-des-w-uuml-r', 'nachrichten', 0);

-- --------------------------------------------------------

--
-- Table structure for table `articleout`
--

CREATE TABLE IF NOT EXISTS `articleout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` longtext,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `autoslag` longtext,
  `slag` varchar(255) DEFAULT NULL,
  `slagtrue` tinyint(1) DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `articleout`
--

INSERT INTO `articleout` (`id`, `body`, `title`, `name`, `locale`, `thumbnail`, `active`, `publish_date`, `autoslag`, `slag`, `slagtrue`, `temp1`, `temp2`, `temp3`) VALUES
(5, '<p>\r\n	Early Life<br />\r\n	<br />\r\n	Steven Paul Jobs was born on February 24, 1955, in San Francisco, California, to Joanne Schieble (later Joanne Simpson) and Abdulfattah &quot;John&quot; Jandali, two University of Wisconsin graduate students who gave their unnamed son up for adoption. His father, Abdulfattah Jandali, was a Syrian political science professor, and his mother, Joanne Schieble, worked as a speech therapist. Shortly after Steve was placed for adoption, his biological parents married and had another child, Mona Simpson. It was not until Jobs was 27 that he was able to uncover information on his biological parents.<br />\r\n	<br />\r\n	As an infant, Steven was adopted by Clara and Paul Jobs and named Steven Paul Jobs. Clara worked as an accountant, and Paul was a Coast Guard veteran and machinist. The family lived in Mountain View, California, within the area that would later become known as&nbsp; Silicon Valley. As a boy, Jobs and his father would work on electronics in the family garage. Paul would show his son how to take apart and reconstruct electronics, a hobby that instilled confidence, tenacity and mechanical prowess in young Jobs.<br />\r\n	<br />\r\n	While Jobs was always an intelligent and innovative thinker, his youth was riddled with frustrations over formal schooling. Jobs was a prankster in elementary school, and his fourth-grade teacher needed to bribe him to study. Jobs tested so well, however, that administrators wanted to skip him ahead to high school&mdash;a proposal that his parents declined.</p>', 'Steve Jobs', 'Steve Jobs', 'gb', NULL, NULL, NULL, 'early-life-steven-paul-jobs-was-born-on-february-24-1955-in-san-francisco-california-to-joanne-schie', 'steve-jobs', 1, NULL, NULL, NULL),
(6, '<h2>\r\n	<span class="mw-headline" id="Early_life">Early life</span></h2>\r\n<p>\r\n	Gates was born in Seattle, Washington, in an upper-middle-class family, the son of William H. Gates, Sr. and Mary Maxwell Gates. His ancestral origin includes English, German, and Scots-Irish.<sup class="reference" id="cite_ref-19"><span>[</span>18<span>]</span></sup><sup class="reference" id="cite_ref-20"><span>[</span>19<span>]</span></sup> His father was a prominent lawyer, and his mother served on the board of directors for First Interstate BancSystem and the United Way. Gates&#39;s maternal grandfather was JW Maxwell, a national bank president. Gates has one elder sister, Kristi (Kristianne), and one younger sister, Libby. He was the fourth of his name in his family, but was known as William Gates III or &quot;Trey&quot; because his father had the &quot;II&quot; suffix.<sup class="reference" id="cite_ref-FOOTNOTEManes199415_21-0"><span>[</span>20<span>]</span></sup> Early on in his life, Gates&#39;s parents had a law career in mind for him.<sup class="reference" id="cite_ref-FOOTNOTEManes199447_22-0"><span>[</span>21<span>]</span></sup> When Gates was young, his family regularly attended a <a href="http://en.wikipedia.org/wiki/Congregational_Christian_Churches" title="Congregational Christian Churches">Congregational</a> church.<sup class="reference" id="cite_ref-Congregational_1_23-0"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-Congregational_1-23"><span>[</span>22<span>]</span></a></sup><sup class="reference" id="cite_ref-Congregational_2_24-0"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-Congregational_2-24"><span>[</span>23<span>]</span></a></sup><sup class="reference" id="cite_ref-Congregational_3_25-0"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-Congregational_3-25"><span>[</span>24<span>]</span></a></sup> The family encouraged competition; one visitor reported that &quot;it didn&#39;t matter whether it was <a href="http://en.wikipedia.org/wiki/Hearts" title="Hearts">hearts</a> or <a href="http://en.wikipedia.org/wiki/Pickleball" title="Pickleball">pickleball</a> or swimming to the dock ... there was always a reward for winning and there was always a penalty for losing&quot;.<sup class="reference" id="cite_ref-nerds2_26-0"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-nerds2-26"><span>[</span>25<span>]</span></a></sup></p>\r\n<p>\r\n	At 13, he enrolled in the <a href="http://en.wikipedia.org/wiki/Lakeside_School" title="Lakeside School">Lakeside School</a>, an exclusive preparatory school.<sup class="reference" id="cite_ref-FOOTNOTEManes199424_27-0"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-FOOTNOTEManes199424-27"><span>[</span>26<span>]</span></a></sup> When he was in the eighth grade, the Mothers Club at the school used proceeds from Lakeside School&#39;s <a class="mw-redirect" href="http://en.wikipedia.org/wiki/Rummage_sale" title="Rummage sale">rummage sale</a> to buy a <a href="http://en.wikipedia.org/wiki/Teletype_Model_33" title="Teletype Model 33">Teletype Model 33</a> ASR terminal and a block of computer time on a <a href="http://en.wikipedia.org/wiki/General_Electric" title="General Electric">General Electric</a> (GE) computer for the school&#39;s students.<sup class="reference" id="cite_ref-FOOTNOTEManes199427_28-0"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-FOOTNOTEManes199427-28"><span>[</span>27<span>]</span></a></sup> Gates took an interest in programming the GE system in <a class="mw-redirect" href="http://en.wikipedia.org/wiki/BASIC_programming_language" title="BASIC programming language">BASIC</a>, and was excused from math classes to pursue his interest. He wrote his first computer program on this machine: an implementation of <a href="http://en.wikipedia.org/wiki/Tic-tac-toe" title="Tic-tac-toe">tic-tac-toe</a> that allowed users to play games against the computer. Gates was fascinated by the machine and how it would always execute software code perfectly. When he reflected back on that moment, he said, &quot;There was just something neat about the machine.&quot;<sup class="reference" id="cite_ref-FOOTNOTEGates199612_29-0"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-FOOTNOTEGates199612-29"><span>[</span>28<span>]</span></a></sup> After the Mothers Club donation was exhausted, he and other students sought time on systems including <a href="http://en.wikipedia.org/wiki/Digital_Equipment_Corporation" title="Digital Equipment Corporation">DEC</a> <a href="http://en.wikipedia.org/wiki/Programmed_Data_Processor" title="Programmed Data Processor">PDP</a> minicomputers. One of these systems was a <a href="http://en.wikipedia.org/wiki/PDP-10" title="PDP-10">PDP-10</a> belonging to Computer Center Corporation (CCC), which banned four Lakeside students&mdash;Gates, <a href="http://en.wikipedia.org/wiki/Paul_Allen" title="Paul Allen">Paul Allen</a>, <a href="http://en.wikipedia.org/wiki/Ric_Weiland" title="Ric Weiland">Ric Weiland</a>, and Kent Evans&mdash;for the summer after it caught them exploiting bugs in the <a href="http://en.wikipedia.org/wiki/Operating_system" title="Operating system">operating system</a> to obtain free computer time.<sup class="reference" id="cite_ref-FOOTNOTEManes199434_30-0"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-FOOTNOTEManes199434-30"><span>[</span>29<span>]</span></a></sup><sup class="reference" id="cite_ref-31"><a href="http://en.wikipedia.org/wiki/Bill_Gates#cite_note-31"><span>[</span>30<span>]</span></a></sup></p>\r\n<p>\r\n	At the end of the ban, the four students offered to find bugs in CCC&#39;s software in exchange for computer time. Rather than use the system via Teletype, Gates went to CCC&#39;s offices and studied <a href="http://en.wikipedia.org/wiki/Source_code" title="Source code">source code</a> for various programs that ran on the system, including programs in <a href="http://en.wikipedia.org/wiki/Fortran" title="Fortran">Fortran</a>, <a href="http://en.wikipedia.org/wiki/Lisp_%28programming_language%29" title="Lisp (programming language)">Lisp</a>, and <a class="mw-redirect" href="http://en.wikipedia.org/wiki/Machine_language" title="Machine language">machine language</a>. The arrangement with CCC continued until 1970, when the company went out of business. The following year, Information Sciences, Inc. hired the four Lakeside students to write a payroll program in <a class="mw-redirect" href="http://en.wikipedia.org/wiki/Cobol" title="Cobol">Cobol</a>, providing them computer time and royalties. After his administrators became aware of his programming abilities, Gates wrote the school&#39;s computer program to schedule students in classes</p>', 'Bill Gates', 'Bill Gates', 'gb', NULL, NULL, NULL, 'early-life-gates-was-born-in-seattle-washington-in-an-upper-middle-class-family-the-son-of-william-h', 'bill-gates', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cat1`
--

CREATE TABLE IF NOT EXISTS `cat1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `langcode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `design` longtext,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`),
  KEY `IDX_5B4F9A2E7294869C` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `cat1`
--

INSERT INTO `cat1` (`id`, `article_id`, `langcode`, `name`, `active`, `position`, `design`, `temp1`, `temp2`, `temp3`) VALUES
(29, 1, 'gb', 'Home', 1, 1, NULL, NULL, NULL, NULL),
(30, 2, 'gb', 'About Us', 1, 2, NULL, NULL, NULL, NULL),
(31, 3, 'gb', 'Links', 1, 17, NULL, NULL, NULL, NULL),
(33, 5, 'gb', 'Contacts', 1, 3, NULL, NULL, NULL, NULL),
(38, 16, 'gb', 'News', 1, 5, 'color:brown;', NULL, NULL, NULL),
(39, 17, 'af', 'afgan menu', 1, 10, NULL, NULL, NULL, NULL),
(40, 18, 'de', 'Hauptseite', 1, 11, NULL, NULL, NULL, NULL),
(42, 25, 'gb', 'Jobs', 0, 13, NULL, NULL, NULL, NULL),
(43, 26, 'pf', 'Principale', 1, 15, NULL, NULL, NULL, NULL),
(44, 27, 'pf', 'Nouvelles', 1, 14, NULL, NULL, NULL, NULL),
(45, 28, 'de', 'Nachrichten', 1, 16, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cat2`
--

CREATE TABLE IF NOT EXISTS `cat2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat1_id` int(11) DEFAULT NULL,
  `ajaxtrue` tinyint(1) DEFAULT NULL,
  `langcode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `design` longtext,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  `article_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C246CB94395F9010` (`cat1_id`),
  KEY `IDX_C246CB947294869C` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `cat2`
--

INSERT INTO `cat2` (`id`, `cat1_id`, `ajaxtrue`, `langcode`, `name`, `active`, `position`, `design`, `temp1`, `temp2`, `temp3`, `article_id`) VALUES
(6, 38, 1, 'gb', 'Supply lines', 1, 2, NULL, NULL, NULL, NULL, 21),
(7, 38, 1, 'gb', 'Missing action', 1, 3, NULL, NULL, NULL, NULL, 22),
(8, 38, 1, 'gb', 'Quit Apple', 1, 1, NULL, NULL, NULL, NULL, 23);

-- --------------------------------------------------------

--
-- Table structure for table `cat3`
--

CREATE TABLE IF NOT EXISTS `cat3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat2_id` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `ajaxtrue` tinyint(1) DEFAULT NULL,
  `langcode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `design` longtext,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`),
  KEY `IDX_B541FB022BEA3FFE` (`cat2_id`),
  KEY `IDX_B541FB027294869C` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `body` longtext,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  `flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `name`, `path`, `body`, `title`, `thumbnail`, `active`, `publish_date`, `temp1`, `temp2`, `temp3`, `flag`) VALUES
(16, NULL, '5480b252c9c11.jpg', 'Australian with the car.', 'Australian', NULL, NULL, '2014-12-04 20:13:22', NULL, NULL, NULL, 1),
(17, NULL, '5490931170d05.jpg', 'Imigrants in action', 'Imigrants', NULL, NULL, '2014-12-16 20:16:17', NULL, NULL, NULL, 1),
(18, NULL, '5490bb4c808a9.jpg', 'Aha aha', 'Aha', NULL, NULL, '2014-12-16 23:07:56', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `username_canonical` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_canonical` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `algorithm` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `address` longtext,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `design` longtext,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `algorithm`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `name`, `surname`, `address`, `active`, `position`, `design`, `temp1`, `temp2`, `temp3`) VALUES
(31, 'admin', 'admin', 'admin@raitis.co.uk', 'admin@raitis.co.uk', 1, 'sha512', '754yhz9130kkokc88s800804sk0o04c', 'd425c83907a8b2345c051a6fc1a136fb195a33052ca4e1607a2c7f4d3172842dabc0b5d3978c7a3933402289dd02968185d3e691ebf35002835d9511fa8a30b3', '2014-11-28 13:46:03', 1, 0, NULL, '6dmknz6ru24okkk08scgssssoogs4kskoo8wgosgoossw8gwc8', '2014-12-16 21:01:30', 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, 'Nick', 'Thorn', 'London', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'test', 'test', 'admin@admin.com', 'admin@admin.com', 1, 'sha512', 'njo8flw4liosck0c8sg0o0k0wosc8gk', 'f2f0498000a5cf02e651104d5c50a4a48be4d6c9a09a996ffdfbe43f2d6559c6f7600b1a3e6486c838caf12a92af9eac3aaedd5d2747cb47702e066132833396', '2014-12-16 23:11:39', 0, 0, NULL, '109ge6woz4rkkgk0w8c40ggowk8g4go44ssks4g0cg4co448c0', '2014-12-04 16:13:24', 'a:2:{i:0;s:16:"ROLE_SUPER_ADMIN";i:1;s:10:"ROLE_ADMIN";}', 0, NULL, 'myname', 'mysurname', 'UK', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lang`
--

CREATE TABLE IF NOT EXISTS `lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `lang`
--

INSERT INTO `lang` (`id`, `name`, `code`, `country`, `active`, `position`, `temp1`, `temp2`, `temp3`) VALUES
(46, 'GB', 'gb', 'GB', 1, 6, NULL, NULL, NULL),
(47, 'FR', 'pf', 'PF', 1, 7, NULL, NULL, NULL),
(48, 'GER', 'de', 'DE', 1, 8, NULL, NULL, NULL),
(49, 'LV', 'lv', 'LV', 0, 9, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `utility`
--

CREATE TABLE IF NOT EXISTS `utility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `utility`
--

INSERT INTO `utility` (`id`, `value`, `active`, `temp1`, `temp2`, `temp3`) VALUES
(1, 'Detect User Locale', 1, NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cat1`
--
ALTER TABLE `cat1`
  ADD CONSTRAINT `FK_5B4F9A2E7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

--
-- Constraints for table `cat2`
--
ALTER TABLE `cat2`
  ADD CONSTRAINT `FK_C246CB94395F9010` FOREIGN KEY (`cat1_id`) REFERENCES `cat1` (`id`),
  ADD CONSTRAINT `FK_C246CB947294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

--
-- Constraints for table `cat3`
--
ALTER TABLE `cat3`
  ADD CONSTRAINT `FK_B541FB022BEA3FFE` FOREIGN KEY (`cat2_id`) REFERENCES `cat2` (`id`),
  ADD CONSTRAINT `FK_B541FB027294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
