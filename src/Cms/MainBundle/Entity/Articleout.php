<?php
// src/Cms/MainBundle/Entity/Article.php

namespace Cms\MainBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="articleout")
 */
class Articleout
{
	
	
	
	
     public function isPasswordLegal()
     {
	   if ($this->getSlagtrue()) {	
         if (!$this->getSlag())
	       return false;
	   }	
     }

	
	
	 
     public function __toString() {
        return $this->getName();
	 }	
	
	
	
	
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


   
   
    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $body;


   
   
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $title;
   
   
   
   
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $name;   
   
   
   
   
   
    /**
     * @ORM\Column(type="string", length=10, nullable="true")
     */
    protected $locale;
	
	
	
	
	   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $thumbnail;
	
	
	


    /**
     * @ORM\Column(type="boolean", nullable="true")
     */
    protected $active;
	
	
	
	

    /**
     * @ORM\Column(type="datetime", nullable="true")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $publish_date;



   
   
    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $autoslag;


   
   
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $slag;
	
		
	


    /**
     * @ORM\Column(type="boolean", nullable="true")
     */
    protected $slagtrue;
	
	
		


    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp1;
	





    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp2;
	
	
	
	

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $temp3;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body
     *
     * @param text $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get body
     *
     * @return text 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set locale
     *
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set publish_date
     *
     * @param datetime $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publish_date = $publishDate;
    }

    /**
     * Get publish_date
     *
     * @return datetime 
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * Set autoslag
     *
     * @param text $autoslag
     */
    public function setAutoslag($autoslag)
    {
        $this->autoslag = $autoslag;
    }

    /**
     * Get autoslag
     *
     * @return text 
     */
    public function getAutoslag()
    {
        return $this->autoslag;
    }

    /**
     * Set slag
     *
     * @param string $slag
     */
    public function setSlag($slag)
    {
        $this->slag = $slag;
    }

    /**
     * Get slag
     *
     * @return string 
     */
    public function getSlag()
    {
        return $this->slag;
    }

    /**
     * Set slagtrue
     *
     * @param boolean $slagtrue
     */
    public function setSlagtrue($slagtrue)
    {
        $this->slagtrue = $slagtrue;
    }

    /**
     * Get slagtrue
     *
     * @return boolean 
     */
    public function getSlagtrue()
    {
        return $this->slagtrue;
    }

    /**
     * Set temp1
     *
     * @param string $temp1
     */
    public function setTemp1($temp1)
    {
        $this->temp1 = $temp1;
    }

    /**
     * Get temp1
     *
     * @return string 
     */
    public function getTemp1()
    {
        return $this->temp1;
    }

    /**
     * Set temp2
     *
     * @param string $temp2
     */
    public function setTemp2($temp2)
    {
        $this->temp2 = $temp2;
    }

    /**
     * Get temp2
     *
     * @return string 
     */
    public function getTemp2()
    {
        return $this->temp2;
    }

    /**
     * Set temp3
     *
     * @param text $temp3
     */
    public function setTemp3($temp3)
    {
        $this->temp3 = $temp3;
    }

    /**
     * Get temp3
     *
     * @return text 
     */
    public function getTemp3()
    {
        return $this->temp3;
    }
}