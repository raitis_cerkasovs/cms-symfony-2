<?php
// src/Cms/MainBundle/Entity/Cat1.php

namespace Cms\MainBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\Table(name="cat2")
 */
class Cat2
{
	
	
	public function __toString() 
	{
     return $this->getName();
	}
	
	
	
	public function __construct()
    {
        $this->cat2s = new ArrayCollection();
    }
	
	
	

    /**
     * @ORM\OneToMany(targetEntity="Cat3", mappedBy="cat2")
	 * @ORM\OrderBy({"position" = "ASC"})
     */
     protected $cat3s;


  
   
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	

	 
    /**
     * @ORM\ManyToOne(targetEntity="Cat1", inversedBy="Cat2s")
     * @ORM\JoinColumn(name="cat1_id", referencedColumnName="id")
     */
    protected $cat1;



    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="Cat2s")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    protected $article;



    /**
     * @ORM\Column(type="boolean", nullable="true")
     */
    protected $ajaxtrue;
	
   
 
     /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $langcode;  
   
   
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $name;
	


    /**
     * @ORM\Column(type="boolean", nullable="true")
     */
    protected $active;
	
	


    /**
     * @ORM\Column(type="integer", nullable="true")
     */
    protected $position;

   
   
   
    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $design;

	


    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp1;
	




    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp2;
	
	
	
	

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $temp3;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ajaxtrue
     *
     * @param boolean $ajaxtrue
     */
    public function setAjaxtrue($ajaxtrue)
    {
        $this->ajaxtrue = $ajaxtrue;
    }

    /**
     * Get ajaxtrue
     *
     * @return boolean 
     */
    public function getAjaxtrue()
    {
        return $this->ajaxtrue;
    }

    /**
     * Set langcode
     *
     * @param string $langcode
     */
    public function setLangcode($langcode)
    {
        $this->langcode = $langcode;
    }

    /**
     * Get langcode
     *
     * @return string 
     */
    public function getLangcode()
    {
        return $this->langcode;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set position
     *
     * @param integer $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set design
     *
     * @param text $design
     */
    public function setDesign($design)
    {
        $this->design = $design;
    }

    /**
     * Get design
     *
     * @return text 
     */
    public function getDesign()
    {
        return $this->design;
    }

    /**
     * Set temp1
     *
     * @param string $temp1
     */
    public function setTemp1($temp1)
    {
        $this->temp1 = $temp1;
    }

    /**
     * Get temp1
     *
     * @return string 
     */
    public function getTemp1()
    {
        return $this->temp1;
    }

    /**
     * Set temp2
     *
     * @param string $temp2
     */
    public function setTemp2($temp2)
    {
        $this->temp2 = $temp2;
    }

    /**
     * Get temp2
     *
     * @return string 
     */
    public function getTemp2()
    {
        return $this->temp2;
    }

    /**
     * Set temp3
     *
     * @param text $temp3
     */
    public function setTemp3($temp3)
    {
        $this->temp3 = $temp3;
    }

    /**
     * Get temp3
     *
     * @return text 
     */
    public function getTemp3()
    {
        return $this->temp3;
    }

    /**
     * Set cat1
     *
     * @param Cms\MainBundle\Entity\Cat1 $cat1
     */
    public function setCat1(\Cms\MainBundle\Entity\Cat1 $cat1)
    {
        $this->cat1 = $cat1;
    }

    /**
     * Get cat1
     *
     * @return Cms\MainBundle\Entity\Cat1 
     */
    public function getCat1()
    {
        return $this->cat1;
    }

    /**
     * Set article
     *
     * @param Cms\MainBundle\Entity\Article $article
     */
    public function setArticle(\Cms\MainBundle\Entity\Article $article)
    {
        $this->article = $article;
    }

    /**
     * Get article
     *
     * @return Cms\MainBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Add cat3s
     *
     * @param Cms\MainBundle\Entity\Cat3 $cat3s
     */
    public function addCat3(\Cms\MainBundle\Entity\Cat3 $cat3s)
    {
        $this->cat3s[] = $cat3s;
    }

    /**
     * Get cat3s
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCat3s()
    {
        return $this->cat3s;
    }
}