<?php
namespace Cms\MainBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\MainBundle\Entity\Article;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;


class DefaultController extends Controller
{
	

    /**
     * Default gateway.
     *
     */
    public function indexAction()
    {
		
	  if ($this->get('session')->getLocale() == 'gp' )
	      $this->setDefLang();
		  
	 
	  $locale = $this->get('session')->getLocale();
	  $entities_cat1 = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array('active'   =>  1, 
	                                                                                           'langcode' => $locale),
	                                                                                     array('position' => 'asc',
																				         )); 	
	  if (!$entities_cat1)	  
        return $this->redirect($this->generateUrl('default_no_menu'));
		 	  
	  if ($entities_cat1[0]->getArticle()->getSlagtrue() < 1)
	    return $this->redirect($this->generateUrl('article_show', array('id'   => $entities_cat1[0]->getArticle()->getId(),
                                                                        'slug' => $entities_cat1[0]->getArticle()->getAutoslag()
																		)));
	  else
	    return $this->redirect($this->generateUrl('article_show', array('id'   => $entities_cat1[0]->getArticle()->getId(),
                                                                        'slug' => $entities_cat1[0]->getArticle()->getSlag()
																		)));
	
    }
	
      
	/**
     * Detect language
     *
     */
	 public function setDefLang()
     {
	     
	  // find first and default language 
	  $entities_lang = $this->getDoctrine()->getRepository('CmsMainBundle:Lang')->findBy(array('active'   =>  1),
	                                                                               array('position' => 'asc')); 	  	 
	  if (!$entities_lang)
	     throw new \Exception('No languages detected');	

	  // set default language
	  $this->get('session')->setLocale($entities_lang[0]->getCode());


      // check for autodiscover locale
      $entities_util = $this->getDoctrine()->getRepository('CmsMainBundle:Utility')->findBy(array('id'   =>  1, 'active' => 1)); 		
       
    
	  if (isset($entities_util[0])):
	     // find user locale and make lowercase	
         $lo = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']);
	   
         // find if locale could be en	 
	  if (strpos($lo, 'gb') !== false or strpos($lo, 'us') !== false or strpos($lo, 'au') !== false or strpos($lo, 'en') !== false)
	    $setengl = true; 		      

		 

	   // go trough locales 
	   foreach ($entities_lang as $entity):
	   
	   // set any en locale
	   if (isset($setengl)) {
			  if ($entity->getCode() == 'gb' or $entity->getCode() == 'us' or $entity->getCode() == 'au') {
		        $this->get('session')->setLocale($entity->getCode());
			  }
		  }
	 
	    //	set locale who mutches	  			  
	    if (strpos($lo, $entity->getCode()) !== false)
		     $this->get('session')->setLocale($entity->getCode());
			 
	   endforeach;
 
       endif;

      }	
	  
	        
	/**
     * If no menu defined. Display "Create first menu."
     *
     */
    public function nomenuAction()
    {
        return $this->render('CmsMainBundle:Default:nomenu.html.twig');
	}

}