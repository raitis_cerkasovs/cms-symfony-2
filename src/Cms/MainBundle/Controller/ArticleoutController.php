<?php

namespace Cms\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\MainBundle\Entity\Articleout;
use Cms\MainBundle\Form\ArticleoutType;
use Cms\MainBundle\Entity\Article;




/**
 * Articleout controller.
 *
 */
class ArticleoutController extends Controller
{
	
    /**
     * Lists all Articleout entities.
     *
     */
    public function indexAction()
    {   $locale = $this->get('session')->getLocale();
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('CmsMainBundle:Articleout')->findAll();

        return $this->render('CmsMainBundle:Articleout:index.html.twig', array(
            'entities' => $entities,
	        'locale'   => $locale,
			'domain'   => $_SERVER['SERVER_NAME']
        ));
    }


    /**
     * Finds and displays a Articleout entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsMainBundle:Articleout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articleout entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsMainBundle:Articleout:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }
	

    /**
     * Displays a form to create a new Articleout entity.
     *
     */
    public function newAction()
    {
        $entity = new Articleout();
	    $form   = $this->createFormBuilder($entity)
			  ->add('body')
			  ->add('slagtrue')
			  ->add('autoslag')
			  ->add('slag')
			  ->add('title')
			  ->add('name')
			  ->add('locale')
              ->getForm();
			  

        return $this->render('CmsMainBundle:Articleout:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
	

    /**
     * Creates a new Articleout entity.
     *
     */
    public function createAction()
    {
        $entity  = new Articleout();
		$locale = $this->get('session')->getLocale();
	    $articleController = new ArticleController();

	    $form   = $this->createFormBuilder($entity)
			  ->add('body')
			  ->add('slagtrue')
			  ->add('autoslag')
			  ->add('slag')
			  ->add('title')
			  ->add('name')
			  ->add('locale')
              ->getForm();
       
	    $request = $this->getRequest();
        $form->bindRequest($request);
			
       if ($form->isValid()) 
		{
	    // make slugs
	    $entity->setAutoslag($articleController->formUrl($entity->getBody()));
	    $entity->    setSlag($articleController->formUrl($entity->getSlag()));	      
	    $entity->  setLocale($locale);	      
	    
	    // make slug for current address
	   if ($entity->getSlagtrue())
		$slug = $articleController->formUrl($entity->getSlag());
	   else
            $slug = $articleController->formUrl($entity->getBody());
	 
	    // save data
	    $em = $this->getDoctrine()->getEntityManager();
        $em->persist($entity);
        $em->flush();
     
	    // return
        return $this->redirect($this->generateUrl('external_show', array('id' => $entity->getId(), 'slug' => $slug)));
        }        	         			  

        return $this->render('CmsMainBundle:Articleout:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'locale' => $locale,
        ));
    }		
		

    /**
     * Displays a form to edit an existing Articleout entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsMainBundle:Articleout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articleout entity.');
        }

        $form  = $this->createFormBuilder($entity)
			  ->add('body')
			  ->add('slagtrue')
			  ->add('autoslag')
			  ->add('slag')
			  ->add('title')
			  ->add('name')
			  ->add('locale')
              ->getForm();
			  
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsMainBundle:Articleout:edit.html.twig', array(
            'entity'      => $entity,
            'form'        => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }


    /**
     * Edits an existing Articleout entity.
     *
     */
    public function updateAction($id)
    {
		$locale = $this->get('session')->getLocale();
        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsMainBundle:Articleout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articleout entity.');
        }

	    $articleController = new ArticleController();

	    $form   = $this->createFormBuilder($entity)
			  ->add('body')
			  ->add('slagtrue')
			  ->add('autoslag')
			  ->add('slag')
			  ->add('title')
			  ->add('name')
			  ->add('locale')
              ->getForm();
       
	    $request = $this->getRequest();
        $form->bindRequest($request);
		
		$deleteForm = $this->createDeleteForm($id);	
 
 
   if ($form->isValid()) 
		{
	    // make slugs
	    $entity->setAutoslag($articleController->formUrl($entity->getBody()));
	    $entity->    setSlag($articleController->formUrl($entity->getSlag()));	      
	    $entity->  setLocale($locale);	      
	 
	    // make slug for current address
	    if ($entity->getSlagtrue())
		    $slug = $articleController->formUrl($entity->getSlag());
	    else
            $slug = $articleController->formUrl($entity->getBody());
	 
	    // save data
	    $em = $this->getDoctrine()->getEntityManager();
        $em->persist($entity);
        $em->flush();
     
	    // return
        return $this->redirect($this->generateUrl('external_show', array('id' => $entity->getId(), 'slug' => $slug)));
        }

        return $this->render('CmsMainBundle:Articleout:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a Articleout entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('CmsMainBundle:Articleout')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Articleout entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('external'));
    }




    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
