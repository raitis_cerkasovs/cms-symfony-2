<?php
namespace Cms\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
	

    /**
     * Render sidebar
     *
     */
    public function adminAction()
    {
	$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array(), array('position' => 'asc')); 
    $locale = $this->get('session')->getLocale();

	   return $this->render('CmsMainBundle:Admin:admin.html.twig', array(
            'entities' => $entities,
			'locale' => $locale
        ));	
	}

}
