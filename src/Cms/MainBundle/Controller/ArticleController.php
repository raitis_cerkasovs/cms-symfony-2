<?php
namespace Cms\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\MainBundle\Entity\Article;
use Cms\MainBundle\Form\ArticleType;


/**
 * Article controller.
 *
 */
class ArticleController extends Controller
{


    /**
     * Show
     *
     */
    public function showAction($id)
    {
		$locale = $this->get('session')->getLocale();
		$vardate = new \DateTime("now");				
		
        $em = $this->getDoctrine()->getEntityManager();
		$entity_a = $em->getRepository('CmsMainBundle:Article')->find(2);
		$entity_b = $em->getRepository('CmsMainBundle:Article')->find(1);
        $entity = $em->getRepository('CmsMainBundle:Article')->find($id);
        $entities_f = $em->getRepository('CmsFileBundle:Document')->findBy(array('flag' => 1), array('publish_date' => 'asc')); 		


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }		

        return $this->render('CmsMainBundle:Article:show.html.twig', array(
            'entity'       => $entity,
            'entities_f'   => $entities_f,
            'entity_a'     => $entity_a,
            'entity_b'     => $entity_b,
			'domain'       => $_SERVER['SERVER_NAME']
        ));
    }
	
	
	/*	
	 * Show Ajax
     *
     */
    public function showAjaxAction($id)
    {
			
        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsMainBundle:Article')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }		

        return $this->render('CmsMainBundle:Article:showAjax.html.twig', array(
            'entity'       => $entity,
        ));
    }


    /**
     * Edit
     *
     */
	  
    public function editAction($id)
    {
		$locale = $this->get('session')->getLocale();
		
        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsMainBundle:Article')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

	   $editForm   = $this->createFormBuilder($entity)
	 		  ->add('body')
	 		  ->add('slagtrue')
			  ->add('autoslag')
			  ->add('slag')
			  ->add('title')
              ->getForm(); 

        return $this->render('CmsMainBundle:Article:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'locale'   => $locale,
       ));
    }
	

    /**
     * Update
     *
     */
    public function updateAction($id)
    {
        $em =      $this->getDoctrine()->getEntityManager();
        $entity =  $em->getRepository('CmsMainBundle:Article')->find($id);
		$locale = $this->get('session')->getLocale();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }
		      
	    $editForm   = $this->createFormBuilder($entity)
			  ->add('body')
			  ->add('slagtrue')
			  ->add('autoslag')
			  ->add('slag')
			  ->add('title')
              ->getForm();
        $request = $this->getRequest();
        $editForm->bindRequest($request);
 
 
   if ($editForm->isValid()) 
		{
	    // make slugs
	    $entity->setAutoslag($this->formUrl($entity->getBody()));
	    $entity->setSlag($this->formUrl($entity->getSlag()));	      
	    // make slug for current address
	    if ($entity->getSlagtrue())
		    $slug = $this->formUrl($entity->getSlag());
	    else
            $slug = $this->formUrl($entity->getBody());
	    // save data	
        $em->persist($entity);
        $em->flush();
        // return
            return $this->redirect($this->generateUrl('article_show', array('id' => $id, 'slug' => $slug)));
        }
        	         			  

        return $this->render('CmsMainBundle:Article:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'locale'   => $locale,
        ));
    }
	
	
    /**
     * Convert url
     *
     */
   public function formUrl($url){
	
	$url = strip_tags($url);    
    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
    $url = trim($url, "-");  
	$url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
	$url = strtolower($url);   
    $url = preg_replace('~[^-a-z0-9_]+~', '', $url); 
    $url = preg_replace(array('/nbsp/', '/quot/'), '', $url); 
    $url = preg_replace(array('/--/'), '-', $url); 
	$url = substr($url, 0, 100);
	
	return $url;
  }


}
