<?php

namespace Cms\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class Cat2Type extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('ajaxtrue')
            ->add('langcode')
            ->add('name')
            ->add('active')
            ->add('position')
            ->add('design')
            ->add('temp1')
            ->add('temp2')
            ->add('temp3')
            ->add('cat1')
            ->add('article')
        ;
    }

    public function getName()
    {
        return 'cms_mainbundle_cat2type';
    }
}
