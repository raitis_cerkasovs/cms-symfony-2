<?php

namespace Cms\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ArticleoutType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('body')
            ->add('title')
            ->add('name')
            ->add('locale')
            ->add('thumbnail')
            ->add('active')
            ->add('publish_date')
            ->add('autoslag')
            ->add('slag')
            ->add('slagtrue')
            ->add('temp1')
            ->add('temp2')
            ->add('temp3')
        ;
    }

    public function getName()
    {
        return 'cms_mainbundle_articleouttype';
    }
}
