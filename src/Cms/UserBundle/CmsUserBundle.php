<?php

namespace Cms\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CmsUserBundle extends Bundle
{	

    public function getParent()
    {
        return 'FOSUserBundle';
    }


}
