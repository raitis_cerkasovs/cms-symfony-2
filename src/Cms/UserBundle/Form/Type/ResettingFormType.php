<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cms\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilder;
use FOS\UserBundle\Form\Type\ResettingFormType as BaseType;

class ResettingFormType extends BaseType
{
   
    public function buildForm(FormBuilder $builder, array $options)
    {
		parent::buildForm($builder, $options);

        $builder->setAttribute('validation_groups', array('ResetPassword_new'));

    }



    public function getName()
    {
        return 'cms_user_resetting';
    }
}
