<?php

namespace Cms\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserType extends AbstractType
{
  public function buildForm(FormBuilder $builder, array $options)
 
 
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('username')
            ->add('email')
            ->add('enabled')
            ->add('locked');

    }

    public function getName()
    {
        return 'cms_userbundle_usertype';
    }
	
	
	
	public function getDefaultOptions(array $options)
   {
    return array(
        'validation_groups' => array('registration_new')
    );
   }



}
